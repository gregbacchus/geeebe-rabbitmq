import { Counter, Summary } from 'prom-client';

/**
 * Custom prometheus metric for counting number of RabbitMQ connection failures and/or disconnects
 */
export const connectionFailures = new Counter({
  help: 'Number of connection failures to from RabbitMQ',
  labelNames: ['kind'],
  name: 'rabbitmq_client_failures',
});

export const messageTiming = new Summary({
  help: 'RabbitMQ message processing timing (seconds)',
  labelNames: ['status'],
  name: 'rabbitmq_message',
});
