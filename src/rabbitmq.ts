import { Time } from '@geeebe/common';
import { logger, Logger } from '@geeebe/logging';
import { compose, Connected, Listener, Middleware, ReceiveQueue, RequestHandler, SendQueue } from '@geeebe/queue';
import { Channel, connect, Connection, ConsumeMessage, Options } from 'amqplib';
import { connectionFailures, messageTiming } from './prometheus';

type ReconnectTime = (attempts: number) => number;

export interface RabbitMqOptions {
  logger?: Logger;
  onConnected: (rabbit: RabbitMQ) => Promise<void>;
  reconnectTime?: number | ReconnectTime;
  url: string | Options.Connect;
}

export interface ReceiveOptions {
  prefetchCount?: number;
  queueOptions?: Options.AssertQueue;
}

export interface RabbitMQQueueTarget {
  queue: string;
}

export interface RabbitMQExchangeTarget {
  exchange: string;
  routingKey: string;
}

export type RabbitMQTarget = RabbitMQQueueTarget | RabbitMQExchangeTarget;

function isQueueTarget(target: RabbitMQTarget): target is RabbitMQQueueTarget {
  return 'queue' in target;
}

/**
 * RabbitMQ connection
 */
export class RabbitMQ implements ReceiveQueue<ReceiveOptions, ConsumeMessage>, SendQueue<RabbitMQTarget, Options.Publish> {
  public get isConnected(): boolean { return !!this.connection; }
  public readonly logger: Logger;

  private connection: Connection | undefined;
  private started = false;
  private reconnectTimer: NodeJS.Timeout | undefined;
  private connectionAttempts: number = 0;
  private middleware: Array<Middleware<ConsumeMessage>> = [];

  constructor(
    private readonly options: RabbitMqOptions,
  ) {
    this.logger = this.options.logger ?? logger.child({ module: 'rabbitmq' });
  }

  /**
   * Connect to server
   */
  public async start(): Promise<void> {
    this.started = true;
    this.createConnection();
  }

  /**
   * Disconnect from server
   */
  public async stop(): Promise<void> {
    if (this.reconnectTimer !== undefined) {
      clearTimeout(this.reconnectTimer);
    }
    this.started = false;
    const connection = this.connection;
    if (!connection) return;
    await connection.close();
    this.logger('Stopped');
  }

  public use(fn: Middleware<ConsumeMessage>): void {
    this.middleware.push(fn);
  }

  /**
   * Start receiving messages from the server
   * @param options receive options
   */
  public async listener(options?: ReceiveOptions): Promise<Listener> {
    const { queueOptions, prefetchCount = 10 } = options ?? {};

    if (!this.connection) throw new Error('Not connected');

    const channel = await this.connection.createChannel();
    channel.on('error', (error) => this.logger('Channel error', { message: error.message }));
    channel.on('close', () => this.logger('Channel closed'));
    channel.on('return', (message) => this.logger('Channel return', { message }));
    channel.on('drain', () => this.logger('Channel drain'));

    await channel.prefetch(prefetchCount);

    const middleware = compose(this.middleware);

    const listen = async (queue: string): Promise<Connected> => {
      // ensure queue exists
      await channel.assertQueue(queue, { durable: true, ...queueOptions });

      // start listening
      const consumer = await channel.consume(queue, this.handleMessage(channel, middleware), { noAck: false });

      this.logger.verbose('Waiting for messages', { ...consumer });
      return {
        disconnect: async () => {
          await channel.cancel(consumer.consumerTag);
        },
      };
    };
    return { listen };
  }

  /**
   * Start receiving messages from the server
   * @param queue name of queue to receive messages from
   * @param options receive options
   */
  public async listen(queue: string, options?: ReceiveOptions): Promise<Connected> {
    const listener = await this.listener(options);
    return await listener.listen(queue);
  }

  /**
   * Send a message to the server
   * @param queue name of the queue to send messages to
   * @param content message content object
   * @param options send options
   */
  public async send(target: RabbitMQTarget, content: {}, options?: Options.Publish): Promise<void> {
    if (!this.connection) throw new Error('Not connected');

    const buffer = Buffer.from(JSON.stringify(content));

    const channel = await this.connection.createChannel();
    if (isQueueTarget(target)) {
      await channel.assertQueue(target.queue, { durable: true });
      channel.sendToQueue(target.queue, buffer, options);
    } else {
      channel.publish(target.exchange, target.routingKey, buffer, options);
    }
    await channel.close();
  }

  private createConnection(): void {
    this.logger('Connecting', { url: this.options.url });
    connect(this.options.url)
      .then((connection) => {
        this.connection = connection;

        // connect events for logging and reconnection
        connection.on('error', (error) => this.logger('Connection error', { message: error.message }));
        connection.on('blocked', (reason) => this.logger('Connection blocked', { reason }));
        connection.on('unblocked', () => this.logger('Connection unblocked'));
        connection.on('close', () => {
          connectionFailures.inc({ kind: 'closed' });
          this.connection = undefined;
          this.logger('Connection closed');
          if (this.started) {
            this.startReconnectTimer();
          }
        });

        this.connectionAttempts = 0;
        this.logger('Connected', { url: this.options.url });
        if (this.options.onConnected) {
          // call the onConnected callback
          this.options.onConnected(this).catch((err) => {
            // if there is an error in onConnected, disconnect and try again
            this.logger.error(err);
            connection.close().catch(this.logger.error);
            this.connection = undefined;
            this.startReconnectTimer();
          });
        }
      })
      .catch((err) => {
        connectionFailures.inc({ kind: 'connect_failed' });
        this.logger('RabbitMQ connection failed', { message: err.message });

        // set retry timer
        this.startReconnectTimer();
      });
  }

  private get reconnectTime(): number {
    if (typeof this.options.reconnectTime === 'function') {
      return this.options.reconnectTime(this.connectionAttempts);
    }
    return this.options.reconnectTime = Time.seconds(5);
  }

  private startReconnectTimer() {
    if (this.reconnectTimer !== undefined) {
      clearTimeout(this.reconnectTimer);
    }
    const reconnectIn = this.reconnectTime;
    this.reconnectTimer = setTimeout(() => {
      if (!this.started) return;

      this.createConnection();
      this.reconnectTimer = undefined;
    }, reconnectIn);

    this.logger(`Reconnect in ${Time.toSeconds(reconnectIn)}s`, { url: this.options.url });
    this.connectionAttempts++;
  }

  private handleMessage = (channel: Channel, middleware: RequestHandler<ConsumeMessage, void>) =>
    (message: ConsumeMessage | null) => {
      if (!message) return;

      const endTimer = messageTiming.startTimer();
      middleware(message, (err) => {
        endTimer({ status: err ? 'error' : 'ok' });
        if (!err) {
          try {
            channel.ack(message);
          } catch (ackErr) {
            this.logger.error(ackErr, { ...message.fields });
          }
          return;
        }
        const requeue = !message.fields.redelivered;
        this.logger.error(err, { requeue, ...message.fields });
        try {
          channel.nack(message, false, requeue);
        } catch (nackErr) {
          this.logger.error(nackErr, { ...message.fields });
        }
      });
    }
}
