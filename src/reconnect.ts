import { Time } from '@geeebe/common';

export const exponentialBackOff = (max: number = Time.minutes(1)) => (attempts: number) => {
  const time = Time.seconds(Math.pow(2, attempts));
  return time > max ? max : time;
};

const DEFAULT_STEPS = [
  Time.seconds(1),
  Time.seconds(1),
  Time.seconds(5),
  Time.seconds(10),
  Time.seconds(30),
  Time.seconds(60),
];

export const steppedBackOff = (steps = DEFAULT_STEPS) => (attempts: number) => {
  if (attempts < 0) return steps[0];
  if (attempts >= steps.length) return steps[steps.length - 1];
  return steps[attempts];
};
